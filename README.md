# Documentation depuis les tests

Les tests d'un projet visent 3 objectifs :

* La documentation fonctionnelle du projet
* La non régression
* Faire émerger le code

Il existe différentes façons d'utiliser les tests pour documenter un projet. 

* Utiliser des noms de méthode explicite évoquant avec précision les différentes règles métier.
* Construire un DSL de test avec le vocabulaire du métier.
* Utiliser du Gherkin pour décrire les scénarios de tests
* Générer des fichiers structurés de documentation depuis les tests

C'est cette dernière option que nous allons faire aujourd'hui.

## Génération de Markdown

Écrire le test suivant et le faire passer au vert.

```java
@Test
void should_add_integers() {
    assertEquals("3", Calculator.add(1, 2));
}
```

Transformer le test pour faire apparaitre l'opération réalisée :

```java
@Test
void should_add_integers() {
    assertEquals("1 + 2 = 3", "1 + 2 = " + Calculator.add(1, 2));
}
```

Transformer le test pour produire un fichier markdown

```java
@Test
void should_add_integers() throws IOException {
    Path path = Paths.get("target", "calculator.md");
    String markdown = "" +
            "# Calculator\n" +
            "## Add\n";
    String operation = "1 + 2 = ";
    String expected = operation + "3";
    assertEquals(expected, operation + Calculator.add(1, 2));
    markdown += expected;
    Files.write(path, markdown.getBytes());
}
```

Regarder le fichier `target/calculator.md`

Restructurer le test pour obtenir un code propre.

Ajouter un nouveau test qui vérifie qu'il n'est pas possible d'additionner `1` à `Integer.MAX_VALUE`.
 Nous souhaitons que les tests produisent le texte suivant dans le fichier Markdown :

```markdown
# Calculator
## Add 2 integers

It's possible to add 2 integers like :

    1 + 3 = 4
    17342 + 62356 = 79698

The result must be lower than the integer max value, 
if the result of addition is greater than 2147483647, 
then the method `Calculator.Add` return "To big"

```

## Une caisse de bar restaurant

L'objectif maintenant est de produire une librairie java et sa documentation pour calculer le bon taux de VAT à appliquer sur un ensemble de lignes de ticket de caisse en fonction du lieu de vente.

Notre calculateur de VAT aura la signature suivante :

```java
public class VatCalculator {
    public VatCalculator(PointOfSale pointOfSale);
    public List<TicketLine> compute(List<CartItem> lines);
}
```

Vous trouverez un ensemble de classes à compléter pour faire fonctionner le projet.

Implémenter en TDD toutes les classes de notre projet accompagné d'une documentation en Markdown de toutes les règles de calcul de VAT applicable dans un bar restaurant français. Chaque règle de calcul de VAT sera accompagné d'au moins un ou deux exemples explicites.

Vous trouverez l'ensemble de ces règles sur les sites suivants :
* https://entreprendre.service-public.fr/vosdroits/F23567
* https://bofip.impots.gouv.fr/bofip/7204-PGP.html

## Étape 2 : Le cas des menus

Un menu est un lot de marchandises et en france depuis le cas Free, les lots doivent être ventilés en respectant les proratas des prix hors lot. 

Ajouter la possibilité de faire des menus et d'afficher les taux de VAT du menu. 
Vous pouvez modifier `CartItem` pour qu'il contienne un ensemble de `CartItem`

Ex 1:
Étant donné la commande suivante prise à la carte :

| Produit       | Prix TTC | VAT | Ratio sur la facture |
|---------------|----------|-----|----------------------|
| Poulet grillé | 12€      | 10% | 60%                  |
| Bière du mois | 8€       | 20% | 40%                  |

Avec les mêmes produits prix dans un menu sur place à 15€ boisson comprise, le plat TTC doit représenter 60% des 15€ et la boisson TTC doit représenter 40% des 15€ donc le ticket de caisse doit indiquer :

```
Menu boisson comprise : 15€ TTC 
  dont VAT 10% : 0,82€
       VAT 20% : 1,00€
```

En effet : 
* 15€ × 60% = 9€ TTC donc 8.18€ HT soit 0,82€ de VAT à 10%
* 15€ × 40% = 6€ TTC donc 6.00€ HT soit 1,00€ de VAT à 20%

Ex 2:
Étant donné la commande suivante prise à la carte :

| Produit            | Prix TTC | VAT | Ratio sur la facture |
|--------------------|----------|-----|----------------------|
| Carbonade Flamande | 17€      | 10% | 85%                  |
| Chti cola          | 3€       | 10% | 15%                  |

Avec les mêmes produits prix dans un menu sur place à 15€ boisson comprise, le plat TTC doit représenter 60% des 15€ et la boisson TTC doit représenter 40% des 15€ donc le ticket de caisse doit indiquer :

```
Menu boisson comprise : 15€ TTC 
  dont VAT 10% : 1,36€
```

En effet :
* 15€ × 85% = 14.45€ TTC donc 12.75€ HT soit 1.16€ de VAT à 10%
* 15€ × 15% = 2.25€ TTC donc 2.05€ HT soit 0.20€ de VAT à 10%

## Étape 3 : Le cas des notes de frais

Au moment de payer l'addition, certains payent exactement ce qu'ils ont pris, les autres divisent l'addition en parts égales, la VAT aussi. Ajouter l'édition de notes de frais à votre API.


