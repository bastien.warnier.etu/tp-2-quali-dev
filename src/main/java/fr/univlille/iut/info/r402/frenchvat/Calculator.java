package fr.univlille.iut.info.r402.frenchvat;

public class Calculator {
	public static String add(int i, int j) {
		if(i >= Integer.MAX_VALUE || j >= Integer.MAX_VALUE) return "Too big";
		return "" + (i+j);
	}
}
