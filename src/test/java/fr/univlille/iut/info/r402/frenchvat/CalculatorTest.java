package fr.univlille.iut.info.r402.frenchvat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

class CalculatorTest {

	/*@Test
	void should_add_integers() {
	    assertEquals("3", Calculator.add(1, 2));
	}
	
	@Test
	void should_add_integers() {
	    assertEquals("1 + 2 = 3", "1 + 2 = " + Calculator.add(1, 2));
	}*/

	
	@Test
	void should_add_integers() throws IOException {
	    Path path = Paths.get("target", "calculator.md");
	    String markdown = "" +
	            "# Calculator\n" +
	            "## Add\n" +
	            "It's possible to add 2 integers like :\n\n";
	    String operation1 = "1 + 3 = ";
	    String expected1 = operation1 + "4";
	    String operation2 = "17342 + 62356 = ";
	    String expected2 = operation2 + "79698";
	    assertEquals(expected1, operation1 + Calculator.add(1, 3));
	    assertEquals(expected2, operation2 + Calculator.add(17342, 62356));
	    markdown += "\t" + expected1 + "\n\t" +expected2 + "\n\n";
	    markdown += "The result must be lower than the integer max value,\n" + 
	    		"if the result of addition is greater than 2147483647,\n" + 
	    		"then the method `Calculator.Add` return \"Too big\"";
	    Files.write(path, markdown.getBytes());
	}

	@Test
	void to_big_add() throws IOException {
	    assertEquals("Too big", Calculator.add(1, Integer.MAX_VALUE));
	}

}
